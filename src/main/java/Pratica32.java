public class Pratica32 {

    public static void main(String[] args) {
        double d = densidade(-1, 67, 3);
        System.out.println(d);
    }

    public static double densidade(double x, double media, double desvio) {
        double d = Math.exp(-0.5*Math.pow(((x - media) / desvio), 2))/(Math.sqrt(2 * Math.PI) * desvio);
        return d;
    }
}